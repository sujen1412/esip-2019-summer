# Import basic d3m pipeline modules
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from d3m.metadata.base import ArgumentType, Context
import importlib
import json
from subprocess import check_output

import d3m.primitives.data_transformation.dataset_to_dataframe
import d3m.primitives.data_transformation.column_parser
import d3m.primitives.data_transformation.construct_predictions
import d3m.primitives.data_transformation.extract_columns_by_semantic_types
import d3m.primitives.data_cleaning.imputer
import d3m.primitives.data_transformation.denormalize
import d3m.primitives.data_preprocessing.text_reader

# Create pipeline meta file to indicate the training dataset and problem
meta_json = {
            "problem": "185_baseball_problem",
            "full_inputs": ["185_baseball_dataset"],
            "train_inputs": ["185_baseball_dataset_TRAIN"],
            "test_inputs": ["185_baseball_dataset_TEST"],
            "score_inputs": ["185_baseball_dataset_SCORE"]
        }

pipeline_description = Pipeline(context=Context.TESTING)
pipeline_description.add_input(name='inputs')

# # Step 1: DatasetToDataFrame
step_0 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 2: ColumnParser
step_1 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.column_parser.DataFrameCommon.metadata.query())
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_output('produce')
pipeline_description.add_step(step_1)

# Step 3: imputer
step_2 = PrimitiveStep(primitive_description=d3m.primitives.data_cleaning.imputer.SKlearn.metadata.query())
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                          data=True)
step_2.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
                          data='replace')
step_2.add_output('produce')
pipeline_description.add_step(step_2)

# Step 4: Primitive
name_space = "d3m.primitives.classification.random_forest.SKlearn"
import_namespace = ".".join(name_space.split(".")[:-1])
name_space_class = importlib.import_module(import_namespace)

step_3 = PrimitiveStep(primitive_description=name_space_class.SKlearn.metadata.query())
step_3.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                          data=True)
step_3.add_hyperparameter(name='add_index_columns', argument_type=ArgumentType.VALUE,
                          data=True)
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')

step_3.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')

step_3.add_output('produce')
pipeline_description.add_step(step_3)

# Step 5: ConstructPredictions
step_4 = PrimitiveStep(
    primitive_description=d3m.primitives.data_transformation.construct_predictions.DataFrameCommon.metadata.query())
step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.3.produce')
step_4.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_4.add_output('produce')
pipeline_description.add_step(step_4)

# Final Output
pipeline_description.add_output(name='output predictions', data_reference='steps.4.produce')

pipeline_description.to_json_structure()

with open("output_pipelines/pipeline-demo.json", 'w') as fw:
    fw.write(json.dumps(pipeline_description.to_json_structure()))
with open("output_pipelines/pipeline-demo.meta", 'w') as fw:
    fw.write(json.dumps(meta_json))


"""
Needs datasets locally downloaded and scoring file downloded.
This function is inspired by https://gitlab.com/datadrivendiscovery/common-primitives/blob/8b0c07ef137e6b09ba35e89e0bbd3d1f17f71d27/run_pipelines.sh
"""
file = "output_pipelines/pipeline-demo.meta"
dataset_dir = "../datasets"
scoring_file = "scoring-pipeline/f596cd77-25f8-4d4c-a350-bb30ab1e58f6.yml"
pipeline_dir = "output_pipelines"
pipeline_file = str(file).replace(".meta", ".json")
cmdline_args = ["python",
                "-m",
                "d3m",
                "--pipelines-path",
                pipeline_dir,
                "runtime",
                "--datasets",
                dataset_dir,
                "fit-score",
                "--meta",
                file,
                "--pipeline",
                pipeline_file,
                "--scoring-pipeline",
                scoring_file,
               "--output-run",
                "pipeline_runs/pipeline_runs.yml",
               "--output",
                "predictions.csv"]

check_output(cmdline_args)
