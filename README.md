# esip-2019-summer

Material from ESIP 2019 Summer meeting in Tacoma. https://www.esipfed.org/meetings/upcoming-meetings/sm19


To follow the demo, we need a few libraries installed.

## Installation
 * Install D3M core library

         pip install d3m
 * Install common primitives

        pip install git+https://gitlab.com/datadrivendiscovery/common-primitives.git@48c0ebf7bb5b86ee6f00a38ae52546dc4c5b3be5#egg=common-primitives

 * Install sklearn primitives

        pip install git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@aab0f8373dfc08e52a61f21998fe2fd7d8d4860a#egg=sklearn_wrap


## Data

Download the datasets in the `datasets` directory, and unzip the 185_baseball.zip file.


## Running the demo

The [construct_pipeline.py](pipelines/construct_pipeline.py) code contains an example solution pipeline for the 185_baseball dataset.


## Docker image

You could also download the following docker image to have all the dependencies installed.

        docker pull registry.gitlab.com/sujen1412/esip-2019-summer:master
